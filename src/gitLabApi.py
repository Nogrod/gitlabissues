import requests

from src import config
from src.helpers import check_token, check_status


def get_issues() -> list:
    """
    get all issues from a given project from gitlab
    :return: list of all issues
    """
    check_token()

    base_url = "https://gitlab.com/api/v4/projects"

    # list with all issues
    issues = []

    page_number = 1
    num_issues = config.max_num_issues

    # if num_issues and max_num_issues is equal, another get request will start for the next page
    while num_issues == config.max_num_issues:

        # url for get request
        url_opened = f"{base_url}/{config.project_id}/issues?per_page={config.max_num_issues}" \
                     f"&page={page_number}&scope=all&access_token={config.token}"

        # get request and saving the response as response object
        r_opened = requests.get(url=url_opened)

        # check for status code (raise error if it is not 200)
        check_status(r_opened)

        # get issues from get request
        page_issues = r_opened.json()
        num_issues = len(page_issues)
        issues.extend(page_issues)

        page_number += 1

    return issues


if __name__ == "__main__":
    get_issues()
