import requests

from src import config


def get_names(issue: dict) -> str:
    """
    get all the names from the assigned persons in one string
    :param issue: issue from which the names are combined
    :return: combined string
    """
    return " ".join([user["name"] for user in issue["assignees"]])


def seconds_in_hours(data: dict):
    """
    convert seconds into hours
    :param data: data where get converted
    :return:
    """
    for key in data.keys():
        data[key] /= 3600


def check_token():
    """
    check if the token is set, raise Error if not
    :return:
    """
    if not config.token:
        raise ValueError("Token must be set. In 'config.py' file")


def check_status(response: requests.Response):
    """
    checks if the http status is correct, if not raise Error
    :return:
    """
    if response.status_code != 200:
        raise requests.HTTPError(response.text)
