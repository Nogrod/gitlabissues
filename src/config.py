# project id from gitlab project. Is required (here or over the command line)
# the command line argument will overwrite this value
# default value
project_id = ""

# personal token from gitlab. Is required (here or over the command line)
# the command line argument will overwrite this value
# default value
token = ""

# if true, the images are displayed in the browser at the end (is more fancy)
# if the argument "--show" is not set, this value is always True, otherwise False
# the else case you can overwrite here
show_image = True


# if true, at the end the images will be saved in the folder "images"
# if argument "--saveNot" is not set, this value is always False, otherwise False
# the "else" case you can overwrite here
save_images = True

format="png"


max_num_issues = 100
