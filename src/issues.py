import datetime
import os

import plotly.figure_factory as ff
import plotly.graph_objects as go

from src import config
from src.gitLabApi import get_issues
from src.helpers import get_names, seconds_in_hours


def create_images():
    """
    create and save an image for time per assignee and time per label
    :return:
    """
    # get data from gitlab api
    issues = get_issues()

    create_gantt_chart(get_gantt(issues))

    #  filter and rearrange date
    assignee_time = get_assignee_time(issues)
    label_time = get_label_time(issues)

    # create and save charts
    create_pie_chart(assignee_time, file_name="assignee_time", title="time per assignee in h")
    create_figure_chart(label_time, file_name="label_time", title="time per label", x_text="labels")


def get_gantt(issues: list) -> list:
    relevant_data_issues = []
    for issue in issues:
        if issue['closed_at'] is None:
            print(f"Skip Issue {issue['title']} because it isn't closed yes (for gantt chart)")
            continue
        relevant_data_issues.append({
            "Task": issue["title"],
            "Start": issue["created_at"].split("T")[0],
            "Finish": issue['closed_at'].split("T")[0]
        })

    return relevant_data_issues


def get_label_time(issues: list) -> dict:
    """
    summarize time per label
    :param issues:
    :return: keys = name of labels, values = summarized time per label
    """
    label_time = {}
    for issue in issues:
        for label in issue["labels"]:
            # add time to a label
            if label not in label_time:
                label_time[label] = issue["time_stats"]["total_time_spent"]
                continue

            label_time[label] += issue["time_stats"]["total_time_spent"]

    seconds_in_hours(label_time)
    return label_time


def get_assignee_time(issues: list) -> dict:
    """
    summarize time per assignee
    :param issues:
    :return: keys = name of assignee, values = summarized time per assignee
    """
    assignee_time = {}
    for issue in issues:
        names = get_names(issue)

        # check if issue is assigned
        if not names:
            names = "not assigned"

        if names not in assignee_time:
            assignee_time[names] = int(issue["time_stats"]["total_time_spent"])
            continue

        assignee_time[names] += int(issue["time_stats"]["total_time_spent"])

    # transform seconds in hours
    seconds_in_hours(assignee_time)

    return assignee_time


def create_pie_chart(data: dict, title: str, file_name: str):
    """
    create a chart object for a pie chart, and save it as a svg file
    :param data: data where get illustrated keys=labels, values=values
    :param title: title from the pie chart
    :param file_name: name form the saved svg file
    :return:
    """
    fig = go.Figure(data=[go.Pie(labels=list(data.keys()), values=list(data.values()))])
    fig.update_traces(textinfo="value+percent+label")
    fig.update_layout(title_text=title)

    save_charts(fig, file_name)


def get_sorted_lists(data: dict):
    data = list(data.items())
    data.sort(key=lambda x: x[0])

    labels = [val[0] for val in data]
    values = [val[1] for val in data]
    return labels, values


def create_gantt_chart(data: list):
    fig = ff.create_gantt(data, colors='#93e4c1', height=(400 + 14 * len(data)))
    save_charts(fig, "Gantt Chart")


def create_figure_chart(data: dict, title: str, x_text: str, file_name: str):
    """
    create a chart object for a bar chart, and save it as a svg file
    :param data: data where get illustrated keys=x axis values= y axis
    :param title: title form the bar chart
    :param x_text: label from the x axis
    :param file_name: name from the saved svg file
    :return:
    """
    x, y = get_sorted_lists(data)
    fig = go.Figure(data=[go.Bar(y=y, x=x)])
    fig.update_layout(
        title=title,
        xaxis_title=x_text,
        yaxis_title="time in h",
    )

    save_charts(fig, file_name)


def save_charts(chart: go.Figure, file_name: str):
    """
    save a given chart as an svg image
    :param chart: chart where get saved
    :param file_name: filename
    :return:
    """
    if config.show_image:
        chart.show()

    if not config.save_images:
        return

    # create image
    image = chart.to_image(format=config.format)

    path = os.path.join("images", f"{file_name}_{datetime.date.today()}.{config.format}")

    i = 0
    while os.path.exists(path):
        i += 1
        path = os.path.join("images", f"{file_name}_{datetime.date.today()} ({i}).{config.format}")

    # save image
    with open(path, "wb") as file:
        file.write(image)


if __name__ == "__main__":
    create_images()
