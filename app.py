import yaml

from src.issues import create_images

from src import config


def set_yaml_config():
    with open("config.yml", 'r') as stream:
        try:
            yaml_config = yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)

        config.format = check_input(yaml_config, "format")
        config.save_images = check_input(yaml_config, "save images")
        config.show_image = check_input(yaml_config, "show images")
        config.project_id = check_input(yaml_config, "project Id")
        config.token = check_input(yaml_config, "token")


def check_input(yml_config, data_str):
    configuration = yml_config[data_str]
    if configuration is None:
        raise ValueError(f'Configuration in "config.yml" not defined: {data_str}')
    return configuration


if __name__ == "__main__":
    # configure()
    set_yaml_config()

    # start script
    create_images()
