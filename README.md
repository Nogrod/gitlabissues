# GitlabIssues

This tool creates diagrams of information it gets from issues in a Gitlab project.

Diagrams that are created:
- Gantt Chart
- Time per member (assignee)
- Time per label

The tool creates images that are saved in the "images" folder. If set, these can also be displayed interactively in the browser.

## installation
[Python](https://www.python.org/) 3 (version >= 3.6) is required. Python must also be added to the PATH, this option can be specified during the installation.

Optionally a [virtual environment](https://docs.python.org/3/tutorial/venv.html) can be created for the tool. In this environment the dependencies can be installed.

In addition, the dependencies must be installed with the command:

`pip install -r requirements.txt`


## usage
Before the tool can be used, the token and the project ID MUST be set in the `config.yml`.

The tool can be executed with the following command:

`"python app.py"`

## config.yml
In the file "config.yml" the necessary configuration is set. 
- Project Id: specifies the Gitlab project from which the data should be taken (The Project Id can be found in the Project overview)
- token: a [token](https://gitlab.com/profile/personal_access_tokens) must be given to download the data (scope: "read api" or "api")
- show images (True or False): indicates whether the images are shown in the browser (interactive)
- save images (True or False): indicates whether the images should be saved
- format: specifies in which format the images should be saved
